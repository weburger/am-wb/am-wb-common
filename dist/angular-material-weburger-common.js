/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialWeburgerCommon', [ 
	'ngMaterialWeburger',
	'xeditable', //https://github.com/vitalets/angular-xeditable
]);

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/**
 * 
 */
angular.module('ngMaterialWeburgerCommon')
.filter("trueOrUndefined", function() {
	return function(value) {
		var res = (value === true || value === 'true');
		return res ? res : undefined;
	};
});


/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/**
 * 
 */
angular.module('ngMaterialWeburgerCommon')
/**
 * 
 */

.config(['ngMdIconServiceProvider', function(ngMdIconServiceProvider) {
	ngMdIconServiceProvider
	// Move actions
	.addShape('wb-common-link', ngMdIconServiceProvider.getShape('link'))
	.addShape('wb-common-toolbar', ngMdIconServiceProvider.getShape('more_horiz'))
	.addShape('wb-common-video', ngMdIconServiceProvider.getShape('video_library'))
	.addShape('wb-common-audio', ngMdIconServiceProvider.getShape('audiotrack'))
	
	.addShape('wb-common-moveup', '<path d="M7.41,15.41L12,10.83L16.59,15.41L18,14L12,8L6,14L7.41,15.41Z" />')
	.addShape('wb-common-movedown', '<path d="M7.41,8.58L12,13.17L16.59,8.58L18,10L12,16L6,10L7.41,8.58Z" />')
	;
}])
/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/**
 * 
 */
angular.module('ngMaterialWeburgerCommon')
/**
 * 
 */
.controller('AmWbCommonAudioCtrl', function($scope, $rootScope, $wbUi) {
	var ngModel = $scope.wbModel;

	/*
	 * Listen model
	 */
	$scope.$watch('wbModel',  function() {
		if (angular.isDefined($scope.wbModel)) {
			ngModel = $scope.wbModel;
			if (!angular
					.isDefined(ngModel.file)) {
				ngModel.content = {};
				ngModel.content.url = 'https://www.w3schools.com/html/mov_bbb.ogg';
				ngModel.content.mimeType = 'audio/mpeg';
				ngModel.setting = {};
				ngModel.setting.autoplay = 'false';
				ngModel.setting.controls = 'true';
				ngModel.setting.preload = 'auto';
			}
		}
	});
	// Global functions
});
'use strict';

angular.module('ngMaterialWeburgerCommon')
/**
 * @ngdoc function
 * @name digidociMainApp.controller:DialogmodelCtrl
 * @description # DialogmodelCtrl Controller of the digidociMainApp
 */
.controller('AmWbCommonDialogmodelCtrl', function($scope, $mdDialog, style, model) {
	$scope.model = model;
	$scope.style = style;
	$scope.hide = function() {
		$mdDialog.hide();
	};
	$scope.cancel = function() {
		$mdDialog.cancel();
	};
	$scope.answer = function(a) {
		$mdDialog.hide(a);
	};
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/**
 * 
 */
angular.module('ngMaterialWeburgerCommon')
/**
 * 
 */
.controller('AmWbCommonFeaturesCtrl', function($scope, $wbUi, $window, $resource) {
	var ngModel = $scope.wbModel;

	function addFeature(){
		ngModel.features.push({
			'icon' : 'assignment_turned_in',
			'title' : 'Feature title',
			'text' : 'Feature description.'
		});
	}

	function removeFeature(feature) {
		var index = ngModel.features.indexOf(feature);
		if (index > -1) {
			ngModel.features.splice(index, 1);
		}
	}

	function removeAllFeatures(){
		ngModel.features = [];
	}
	
	function editFeature(feature) {
		$wbUi.openDialog({
			controller : 'AmWbCommonDialogmodelCtrl',
			templateUrl : 'views/am-wb-common-dialogs/feature.html',
			parent : angular.element(document.body),
			clickOutsideToClose : true,
			locals : {
				model : feature,
				style : {
					title : 'service'
				}
			}
		});
	}

	function moveUpFeature(feature) {
		var index = ngModel.features.indexOf(feature);
		if(index == 0){
			return;
		}
		moveArrayItem(ngModel.features, index, index - 1);
	}

	function moveDownFeature(feature) {
		var index = ngModel.features.indexOf(feature);
		if(index == ngModel.features.length - 1){
			return;
		}
		moveArrayItem(ngModel.features, index, index + 1);
	}

	function moveArrayItem(array, old_index, new_index) {
		if (new_index >= array.length) {
			var k = new_index - array.length;
			while ((k--) + 1) {
				array.push(undefined);
			}
		}
		array.splice(new_index, 0, array.splice(old_index, 1)[0]);
	};

	/*
	 * run feature action
	 */
	function runAction(feature) {
		if (feature.action.type==='link') {
			$window.location = feature.action.link;
			return;
		}
	}
	
	/**
	 * Selects featrue image
	 * 
	 * @param feature
	 * @returns
	 */
	function selectImage(feature) {
		return $resource.get('image')//
		.then(function(value){
			feature.image = value;
		});
	}

	/*
	 * Listen model
	 */
	$scope.$watch('wbModel', function() {
		if(angular.isDefined($scope.wbModel)){
			ngModel = $scope.wbModel;
			if (!angular.isDefined(ngModel.features)) {
				ngModel.features = [];
			}
		}
	});

	// Global functions
	$scope.addFeature = addFeature;
	$scope.removeFeature = removeFeature;
	$scope.removeAllFeatures = removeAllFeatures;
	$scope.editFeature = editFeature;
	$scope.moveUpFeature = moveUpFeature;
	$scope.moveDownFeature = moveDownFeature;
	$scope.runAction = runAction;
	$scope.selectImage = selectImage;
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/**
 * 
 */
angular.module('ngMaterialWeburgerCommon')
/**
 * 
 */
.controller('AmWbCommonVideoCtrl', function($scope, $rootScope, $wbUi) {
	var ngModel = $scope.wbModel;

	/*
	 * Listen model
	 */
	$scope.$watch('wbModel',  function() {
		if (angular.isDefined($scope.wbModel)) {
			ngModel = $scope.wbModel;
			if (!angular.isDefined(ngModel.content)) {
				ngModel.content = {};
				ngModel.content.url = 'https://www.w3schools.com/html/mov_bbb.ogg';
				ngModel.content.mimeType = 'video/ogg';
				ngModel.setting = {};
				ngModel.setting.autoplay = 'false';
				ngModel.setting.controls = 'true';
				ngModel.setting.preload = 'auto';
				ngModel.setting.width = '90%';
				ngModel.setting.height = '90%';
			}
		}
	});
	// Global functions
});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialWeburgerCommon')

/**
 * @ngdoc directive
 * @name amWbInline
 * @description # amWbInline
 */
.directive('amWbInline', function($q, $parse, $resource) {

	/**
	 * Link data and view
	 */
	function postLink(scope, attr, elem, ngModel) {
		scope.$watch(function(){
			return ngModel.$modelValue;
		}, function(value) {
			scope.myDataModel = value;
		});
		scope.$watch('myDataModel', function(value){
			ngModel.$setViewValue(value);
		})
		scope.myDataModel = ngModel.$viewValue;
		
		scope.ok = function(d){
			ngModel.$setViewValue(d);
			if(typeof scope.amWbInlineOnSave !== 'undefined'){
				scope.$data = d;
				scope.amWbInlineOnSave(scope);				
			}
		}
		
		/**
		 * Select image url
		 */
		scope.updateImage = function(){
			return $resource.get('image', {
				style : {
					title : 'Select image'
				},
				data : scope.myDataModel
			}) //
			.then(function(url){
				scope.ok(url);
			});
		}
	}
	
	return {
		restrict : 'E',
		transclude : true,
		replace: true,
		require: '^ngModel',
		scope: {
			amWbInlineType: '@',
			amWbInlineEnable: '<',
			amWbInlineLabel: '@?',
			amWbInlineOnSave: '&?'
		},
		templateUrl : 'views/directives/am-wb-inline.html',
		link: postLink
	};
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialWeburgerCommon')

/**
 * Load widgets
 */
.run(function($settings) {
	$settings.newPage({
		type: 'common-action',
		label : 'Action',
		description : 'Set glubal action attribute',
		controller: 'AmWbCommonActionCtrl',
		icon : 'settings',
		templateUrl : 'views/am-wb-common-settings/action.html'
	});
	$settings.newPage({
		type: 'common-features',
		label : 'Features',
		description : 'Manage list of features which are used in the current widget.',
		controller: 'AmWbCommonFeaturesCtrl',
		icon : 'settings',
		templateUrl : 'views/am-wb-common-settings/features.html'
	});
	$settings.newPage({
		type: 'common-audio-player',
		label : 'Audio Player',
		description : 'Manage playing audio in the current widget.',
		icon : 'settings',
		templateUrl : 'views/am-wb-common-settings/audio-player.html'
	});
	$settings.newPage({
		type: 'common-video-player',
		label : 'Video Player',
		description : 'Manage showing video in the current widget.',
		icon : 'settings',
		templateUrl : 'views/am-wb-common-settings/video-player.html'
	});
});

/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialWeburgerCommon')
/**
 * @ngdoc module
 * @name ngDonate
 * @description
 * 
 */
.run(function($widget) {

	$widget.newWidget({
		type: 'CommonFeatureList',
		templateUrl : 'views/am-wb-common-widgets/feature-list.html',
		label : 'Vertical feature list',
		description : 'Vertical feature list',
		icon : 'wb-common-link',
		help : 'https://gitlab.com/weburger/am-wb-common/wikis/home',
		controller: 'AmWbCommonFeaturesCtrl',
		setting:['text', 'common-features', ],
	});

	$widget.newWidget({
		type: 'CommonFeatureLinks',
		templateUrl : 'views/am-wb-common-widgets/feature-links.html',
		label : 'Feature link',
		description : 'Vertical features link list',
		icon : 'wb-common-link',
		help : 'http://dpq.co.ir',
		controller: 'AmWbCommonFeaturesCtrl',
		setting:['text', 'common-features'],
	});

	$widget.newWidget({
		type: 'CommonFeatureMozaic',
		templateUrl : 'views/am-wb-common-widgets/feature-mozaic.html',
		label : 'Feature mozaic',
		description : 'Vertical features mozaic',
		icon : 'wb-common-link',
		help : 'http://dpq.co.ir',
		controller: 'AmWbCommonFeaturesCtrl',
		setting:['text', 'common-features'],
	});

	$widget.newWidget({
		type: 'CommonActionCall',
		templateUrl : 'views/am-wb-common-widgets/action-call.html',
		label : 'Action call',
		description : 'Call action with brand, title and text.',
		icon : 'wb-common-link',
		help : 'http://dpq.co.ir',
		controller: 'AmWbCommonFeaturesCtrl',
		setting:['description', 'common-features',],
	});

	$widget.newWidget({
		type: 'CommonFeatureToolbar',
		templateUrl : 'views/am-wb-common-widgets/feature-toolbar.html',
		label : 'Toolbar',
		description : 'A toolbar to show actions with sidenav.',
		icon : 'wb-common-toolbar',
		help : 'https://gitlab.com/weburger/am-wb-common/wikis/toolbar',
		controller: 'AmWbCommonFeaturesCtrl',
		setting:['description','common-features',],
	});
	
	$widget.newWidget({
		type: 'CommonVideoPlayer',
		templateUrl : 'views/am-wb-common-widgets/video-player.html',
		label : 'Video Player',
		description : 'A video player component.',
		icon : 'wb-common-video',
		help : 'https://gitlab.com/weburger/am-wb-common/wikis/video-player',
		controller: 'AmWbCommonVideoCtrl',
		setting:['common-video-player'],
	});
	$widget.newWidget({
		type: 'CommonAudioPlayer',
		templateUrl : 'views/am-wb-common-widgets/audio-player.html',
		label : 'Audio Player',
		description : 'An audio player component.',
		icon : 'wb-common-audio',
		help : 'https://gitlab.com/weburger/am-wb-common/wikis/audio-player',
		controller: 'AmWbCommonAudioCtrl',
		setting:['common-audio-player'],
	});
});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialWeburgerCommon')
/**
 * تنظیمات مربوط به افزونه xeditable
 * 
 */
.run(function(editableOptions, editableThemes, $rootScope) {
//	editableOptions.theme = 'default'; // Default theme. Can be also 'bs2', 'bs3'
	editableThemes['angular-material'] = {
		formTpl:      '<form class="editable-wrap"></form>',
		noformTpl:    '<span class="editable-wrap"></span>',
		controlsTpl:  '<md-input-container class="editable-controls" ng-class="{\'md-input-invalid\': $error}"></md-input-container>',
		inputTpl:     '',
		errorTpl:     '<div ng-messages="{message: $error}"><div class="editable-error" ng-message="message">{{$error}}</div></div>',
		buttonsTpl:   '<span class="editable-buttons"></span>',
		submitTpl:    '<md-button type="submit" class="md-primary">save</md-button>',
		cancelTpl:    '<md-button type="button" class="md-warn" ng-click="$form.$cancel()">cancel</md-button>'
	};

	editableThemes['angular-material'].submitTpl = '<md-button class="md-icon-button md-primary" ng-click="$form.$submit()"><wb-icon>done<wb-icon></md-button>';
	editableThemes['angular-material'].cancelTpl = '<md-button class="md-icon-button md-warn" ng-click="$form.$cancel()"><wb-icon>cancel<wb-icon></md-button>';
	editableOptions.theme = 'angular-material';
//	editableOptions.isDisabled="false";
	editableOptions.iconSet="font-awesome";
	
});

angular.module('ngMaterialWeburgerCommon').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/am-wb-common-dialogs/feature.html',
    "<md-dialog aria-label=\"Feature item config\" ng-cloak> <md-toolbar> <div class=md-toolbar-tools> <h2 translate>Feature list item</h2> <span flex></span> <md-button class=md-icon-button ng-click=cancel()> <wb-icon aria-label=\"Close dialog\">close</wb-icon> </md-button> </div> </md-toolbar> <md-dialog-content> <md-tabs md-dynamic-height md-border-bottom> <md-tab label=Context> <md-content layout=column>  <wb-ui-setting-image title=Image value=model.image> </wb-ui-setting-image> <md-input-container> <label translate>Title</label> <input ng-model=model.title> </md-input-container> <md-input-container> <label translate>Text</label> <input ng-model=model.text> </md-input-container> </md-content> </md-tab> <md-tab label=Action> <md-content layout=column> <md-input-container> <label translate>Type</label> <input ng-model=model.action.type> </md-input-container> <md-input-container> <label translate>Link (URL)</label> <input ng-model=model.action.link> </md-input-container> <md-input-container> <label translate>Label</label> <input ng-model=model.action.label> </md-input-container> </md-content> </md-tab> </md-tabs> </md-dialog-content> </md-dialog>"
  );


  $templateCache.put('views/am-wb-common-settings/action.html',
    " <md-list class=wb-setting-panel> <md-input-container class=\"md-icon-float md-block\"> <label>Label</label> <input ng-model=wbModel.action.label> </md-input-container> <wb-ui-setting-dropdown title=\"Action type\" icon=border_left items=types value=wbModel.action.type> </wb-ui-setting-dropdown>  <md-input-container ng-show=\"wbModel.action.type=='link'\" class=\"md-icon-float md-block\"> <label>Link</label> <input ng-model=wbModel.action.link> </md-input-container> </md-list>"
  );


  $templateCache.put('views/am-wb-common-settings/audio-player.html',
    "<md-list class=wb-setting-panel>        <wb-ui-setting-audio title=Source value=wbModel.content.url> </wb-ui-setting-audio> <md-input-container class=\"md-icon-float md-block\"> <label>Type</label> <input ng-model=wbModel.content.mimeType aria-label=\"MIME Type\"> </md-input-container> <wb-ui-setting-on-off-switch title=Autoplay? icon=play_arrow value=wbModel.setting.autoplay> </wb-ui-setting-on-off-switch> <wb-ui-setting-on-off-switch title=Controls? icon=swap_horiz value=wbModel.setting.controls> </wb-ui-setting-on-off-switch> <wb-ui-setting-on-off-switch title=Loop? icon=replay value=wbModel.setting.loop> </wb-ui-setting-on-off-switch> <wb-ui-setting-dropdown title=Preload icon=cached items=\"[\n" +
    "\t\t\t\t{'title':'auto','value':'auto'},\n" +
    "\t\t\t\t{'title':'metadata','value':'metadata'},\n" +
    "\t\t\t\t{'title':'none','value':'none'}\n" +
    "\t\t\t]\" value=wbModel.setting.preload> </wb-ui-setting-dropdown> </md-list>"
  );


  $templateCache.put('views/am-wb-common-settings/features.html',
    "<div class=md-toolbar-tools> <span flex></span> <md-button ng-click=addFeature() class=md-icon-button aria-label=\"Add featrue\"> <wb-icon>add</wb-icon> </md-button> <md-button ng-click=removeAllFeatures() class=md-icon-button aria-label=\"Add featrue\"> <wb-icon>clear</wb-icon> </md-button> </div> <md-list> <md-list-item ng-repeat=\"feature in wbModel.features\" ng-click=\"editFeature(feature, $event)\" class=noright> <img ng-src={{feature.image}} width=32px height=32px class=\"md-avatar-icon\"> <p>{{ feature.title }}</p>  <wb-icon ng-show=\"$index<wbModel.features.length-1\" ng-click=\"moveDownFeature(feature, $event)\" class=\"md-secondary md-hue-3\" aria-label=\"Move up\">wb-common-movedown</wb-icon> <wb-icon ng-show=\"$index>0\" ng-click=\"moveUpFeature(feature, $event)\" class=\"md-secondary md-hue-3\" aria-label=\"Move up\">wb-common-moveup</wb-icon> <wb-icon class=md-secondary ng-click=\"removeFeature(feature, $event)\" aria-label=Delete>delete</wb-icon> <wb-icon class=md-secondary ng-click=selectImage(feature) aria-label=\"Set image\">add_a_photo</wb-icon> </md-list-item> </md-list>"
  );


  $templateCache.put('views/am-wb-common-settings/video-player.html',
    "<md-list class=wb-setting-panel>        <wb-ui-setting-video title=Source value=wbModel.content.url> </wb-ui-setting-video> <md-input-container class=\"md-icon-float md-block\"> <label>Type</label> <input ng-model=wbModel.content.mimeType aria-label=\"MIME Type\"> </md-input-container>       <wb-ui-setting-image title=Poster value=wbModel.setting.poster> </wb-ui-setting-image> <wb-ui-setting-on-off-switch title=Autoplay? icon=play_arrow value=wbModel.setting.autoplay> </wb-ui-setting-on-off-switch> <wb-ui-setting-on-off-switch title=Controls? icon=swap_horiz value=wbModel.setting.controls> </wb-ui-setting-on-off-switch> <wb-ui-setting-on-off-switch title=Loop? icon=replay value=wbModel.setting.loop> </wb-ui-setting-on-off-switch> <wb-ui-setting-dropdown title=Preload icon=cached items=\"[\n" +
    "\t\t\t\t{'title':'auto','value':'auto'},\n" +
    "\t\t\t\t{'title':'metadata','value':'metadata'},\n" +
    "\t\t\t\t{'title':'none','value':'none'}\n" +
    "\t\t\t]\" value=wbModel.setting.preload> </wb-ui-setting-dropdown> </md-list>"
  );


  $templateCache.put('views/am-wb-common-widgets/action-call.html',
    "<div layout=column layout-align=\"space-between center\" layout-align-gt-xs=\"center center\"> <h1 class=md-display-3 ng-style=\"{'color': wbModel.style.color, 'text-align':'center'}\" ng-class=\"{'dd-rtl':wbModel.style.rtl}\"> {{ wbModel.label }} </h1> <span></span> <h4 class=md-display-1 ng-style=\"{'color': wbModel.style.color, 'text-align':'center'}\" ng-class=\"{'dd-rtl':wbModel.style.rtl}\" hide show-gt-xs>{{wbModel.description}}</h4> <div ng-style=\"{'width': '80%'}\" layout=column layout-gt-xs=row layout-align-gt-xs=\"center center\"> <md-button ng-repeat=\"feature in wbModel.features\" ng-style=\"{'color': wbModel.style.color, 'font-size': '20px', 'padding':'5px 20px'}\" class=\"md-button md-raised md-primary\" ng-href={{wbModel.action.link}} ng-click=event.preventDefault();runAction(feature)> {{ feature.title }} </md-button> </div> </div>"
  );


  $templateCache.put('views/am-wb-common-widgets/audio-player.html',
    " <div layout=row layout-wrap layout-align=\"center center\"> <audio style=width:90% ng-attr-autoplay=\"{{wbModel.setting.autoplay | trueOrUndefined}}\" ng-attr-controls=\"{{wbModel.setting.controls | trueOrUndefined}}\" ng-attr-loop=\"{{wbModel.setting.loop | trueOrUndefined}}\" poster=images/wallpaper.jpg preload={{wbModel.setting.preload}}> <source src={{wbModel.content.url}} type=\"{{wbModel.content.mimeType}}\"> Your browser does not support the video tag. It seems your browser is too old! </audio> </div>"
  );


  $templateCache.put('views/am-wb-common-widgets/feature-links.html',
    " <div layout=column> <div ng-repeat=\"link in wbModel.features\"> <md-button class=md-primary ng-click=runAction(link) ng-href={{link.action.link}}> <md-icon ng-if=link.action.icon>{{link.icon}}</md-icon> {{link.title}} </md-button> <md-button ng-if=wbEditable class=md-icon-button ng-click=editFeature(link)> <md-icon ng-style=\"{ 'color': wbModel.style.color }\">edit</md-icon> </md-button> <md-button ng-if=wbEditable class=md-icon-button ng-click=deleteFeature(link)> <md-icon ng-style=\"{ 'color': wbModel.style.color }\">delete</md-icon> </md-button> </div> <div> <md-button ng-if=wbEditable aria-label=\"add feature\" ng-click=addFeature(wbModel)> <md-icon ng-style=\"{ 'color': wbModel.style.color }\">add</md-icon> </md-button> </div> </div>"
  );


  $templateCache.put('views/am-wb-common-widgets/feature-list.html',
    " <div ng-hide=isSelected() ng-bind-html=\"wbModel.text | wbunsafe\"> </div> <div ui-tinymce=\"{\n" +
    "\t\tselector : 'div.tinymce',\n" +
    "\t\ttheme : 'inlite',\n" +
    "\t\tplugins : 'directionality contextmenu table link paste image imagetools hr textpattern autolink textcolor colorpicker ',\n" +
    "\t\tinsert_toolbar : 'quickimage quicktable',\n" +
    "\t\tselection_toolbar : 'bold italic | quicklink h1 h2 h3 blockquote | ltr rtl | forecolor',\n" +
    "\t\tinsert_button_items: 'image link | inserttable | hr',\n" +
    "\t\tinline : true,\n" +
    "\t\tpaste_data_images : true,\n" +
    "\t\tbranding: false,\n" +
    "\t\timagetools_toolbar: 'rotateleft rotateright | flipv fliph | editimage imageoptions'\n" +
    "\t}\" ng-model=wbModel.text ng-show=isSelected() flex> </div> <div ng-class=\"{'dd-rtl':wbModel.style.rtl}\" layout=column layout-align=\"start center\" layout-gt-sm=row layout-align-gt-sm=\"space-around center\" ng-style=\"{'width': '100%'}\"> <div layout=column layout-align=\"center center\" layout-padding ng-repeat=\"s in wbModel.features\" ng-class=\"{'feature-list-link': s.action.type}\" ng-click=\"!wbEditable && runAction(s)\"> <img width=128px height=128px ng-src={{s.image}}> <h3>{{s.title}}</h3> <div ng-hide=isSelected() ng-bind-html=\"s.text | wbunsafe\"> </div> <div ui-tinymce=\"{\n" +
    "\t\t\t\tselector : 'div.tinymce',\n" +
    "\t\t\t\ttheme : 'inlite',\n" +
    "\t\t\t\tplugins : 'directionality contextmenu table link paste image imagetools hr textpattern autolink textcolor colorpicker ',\n" +
    "\t\t\t\tinsert_toolbar : 'quickimage quicktable',\n" +
    "\t\t\t\tselection_toolbar : 'bold italic | quicklink h1 h2 h3 blockquote | ltr rtl | forecolor',\n" +
    "\t\t\t\tinsert_button_items: 'image link | inserttable | hr',\n" +
    "\t\t\t\tinline : true,\n" +
    "\t\t\t\tpaste_data_images : true,\n" +
    "\t\t\t\tbranding: false,\n" +
    "\t\t\t\timagetools_toolbar: 'rotateleft rotateright | flipv fliph | editimage imageoptions'\n" +
    "\t\t\t}\" ng-model=s.text ng-show=isSelected() flex> </div> </div> <md-button ng-if=wbEditable aria-label=\"add social\" ng-click=addFeature()> <wb-icon>add</wb-icon> </md-button> </div>"
  );


  $templateCache.put('views/am-wb-common-widgets/feature-mozaic.html',
    " <div layout=row layout-wrap layout-padding> <div layout=column layout-align=\"center start\" flex=100 flex-gt-md=50 ng-repeat=\"tile in wbModel.features\"> <h3>{{tile.title}}</h3> <div ng-if=wbEditable> <md-button class=md-icon-button ng-click=editFeature(tile)> <md-icon ng-style=\"{ 'color': wbModel.style.color }\">edit</md-icon> </md-button> <md-button class=md-icon-button ng-click=deleteFeature(tile)> <md-icon ng-style=\"{ 'color': wbModel.style.color }\">delete</md-icon> </md-button> </div> <p class=md-body-1>{{tile.text}}</p> <md-button class=md-primary ng-click=runAction(tile) ng-href={{tile.action.link}}> <md-icon ng-if=tile.action.icon>{{tile.icon}}</md-icon> {{tile.action.label}} </md-button> </div> <md-button ng-if=wbEditable aria-label=\"add social\" ng-click=addFeature()> <md-icon ng-style=\"{ 'color': wbModel.style.color }\">add</md-icon> </md-button> </div>"
  );


  $templateCache.put('views/am-wb-common-widgets/feature-toolbar.html',
    "<md-toolbar wb-border=wbModel.style layout=row layout-align=\"center center\"> <img ng-if=wbModel.cover height=42px ng-src=\"{{ wbModel.cover }}\" ng-class=\"md-icon-button\"> <md-truncate ng-if=wbModel.label layout-padding> {{ wbModel.label }}</md-truncate> <md-button hide show-gt-md ng-repeat=\"feature in wbModel.features | limitTo: 8\" aria-label={{feature.title}} ng-click=runAction(feature) ng-class=\"{'md-icon-button': feature.image}\"> <img ng-if=feature.image height=32px ng-src=\"{{ feature.image }}\"> <span ng-if=!feature.image>{{ feature.title }}</span> <md-tooltip ng-if=feature.text>{{ feature.text }}</md-tooltip> </md-button> <span flex></span> <md-menu ng-show=wbModel.features.length> <md-button aria-label=\"Open demo menu\" class=md-icon-button ng-click=$mdMenu.open($event)> <wb-icon>more_vert</wb-icon> </md-button> <md-menu-content> <md-menu-item ng-repeat=\"feature in wbModel.features\"> <md-button show-gt-md aria-label=menu ng-click=runAction(feature)> <img height=24px ng-src=\"{{ feature.image }}\"> {{ feature.title }} </md-button> </md-menu-item> </md-menu-content> </md-menu> </md-toolbar>"
  );


  $templateCache.put('views/am-wb-common-widgets/video-player.html',
    " <div layout=row layout-wrap layout-align=\"center center\"> <video width={{wbModel.setting.width}} height={{wbModel.setting.height}} ng-attr-autoplay=\"{{wbModel.setting.autoplay | trueOrUndefined}}\" ng-attr-controls=\"{{wbModel.setting.controls | trueOrUndefined}}\" ng-attr-loop=\"{{wbModel.setting.loop | trueOrUndefined}}\" poster={{wbModel.setting.poster}} preload={{wbModel.setting.preload}}> <source src={{wbModel.content.url}} type=\"{{wbModel.content.mimeType}}\"> Your browser does not support the video tag. It seems your browser is too old! </video> </div>"
  );


  $templateCache.put('views/directives/am-wb-inline.html',
    "<div ng-switch=amWbInlineType>  <div ng-switch-when=image ng-click=updateImage() ng-class=\"{'my-editable' : $parent.amWbInlineEnable}\" md-colors=\"::{borderColor: 'primary-100'}\"> <ng-transclude flex></ng-transclude> <div class=xedit-icon ng-show=$parent.amWbInlineEnable md-colors=\"::{color: 'primary',backgroundColor: 'primary-100', borderColor: 'primary-100'}\"> <wb-icon>edit</wb-icon> </div> </div>  <div ng-switch-when=text editable-text=$parent.myDataModel onaftersave=$parent.ok($data) layout=row ng-class=\"{'my-editable' : $parent.amWbInlineEnable}\" md-colors=\"::{borderColor: 'primary-100'}\"> <ng-transclude flex></ng-transclude> <div class=xedit-icon ng-show=$parent.amWbInlineEnable md-colors=\"::{color: 'primary',backgroundColor: 'primary-100', borderColor: 'primary-100'}\"> <wb-icon>edit</wb-icon> </div> </div>  <div ng-switch-when=textarea editable-textarea=$parent.myDataModel onaftersave=$parent.ok($data) layout=row ng-class=\"{'my-editable' : $parent.amWbInlineEnable}\" md-colors=\"::{borderColor: 'primary-100'}\"> <ng-transclude flex></ng-transclude> <div class=xedit-icon ng-show=$parent.amWbInlineEnable md-colors=\"::{color: 'primary',backgroundColor: 'primary-100', borderColor: 'primary-100'}\"> <wb-icon>edit</wb-icon> </div> </div>  <div ng-switch-when=email editable-email=$parent.myDataModel onaftersave=$parent.ok($data) layout=row ng-class=\"{'my-editable' : $parent.amWbInlineEnable}\" md-colors=\"::{borderColor: 'primary-100'}\"> <ng-transclude flex></ng-transclude> <div class=xedit-icon ng-show=$parent.amWbInlineEnable md-colors=\"::{color: 'primary',backgroundColor: 'primary-100', borderColor: 'primary-100'}\"> <wb-icon>edit</wb-icon> </div> </div>  <div ng-switch-when=tel editable-tel=$parent.myDataModel onaftersave=$parent.ok($data) layout=row ng-class=\"{'my-editable' : $parent.amWbInlineEnable}\" md-colors=\"::{borderColor: 'primary-100'}\"> <ng-transclude flex></ng-transclude> <div class=xedit-icon ng-show=$parent.amWbInlineEnable md-colors=\"::{color: 'primary',backgroundColor: 'primary-100', borderColor: 'primary-100'}\"> <wb-icon>edit</wb-icon> </div> </div>  <div ng-switch-when=number editable-number=$parent.myDataModel onaftersave=$parent.ok($data) layout=row ng-class=\"{'my-editable' : $parent.amWbInlineEnable}\" md-colors=\"::{borderColor: 'primary-100'}\"> <ng-transclude flex></ng-transclude> <div class=xedit-icon ng-show=$parent.amWbInlineEnable md-colors=\"::{color: 'primary',backgroundColor: 'primary-100', borderColor: 'primary-100'}\"> <wb-icon>edit</wb-icon> </div> </div>  <div ng-switch-when=range editable-range=$parent.myDataModel onaftersave=$parent.ok($data) layout=row ng-class=\"{'my-editable' : $parent.amWbInlineEnable}\" md-colors=\"::{borderColor: 'primary-100'}\"> <ng-transclude flex></ng-transclude> <div class=xedit-icon ng-show=$parent.amWbInlineEnable md-colors=\"::{color: 'primary',backgroundColor: 'primary-100', borderColor: 'primary-100'}\"> <wb-icon>edit</wb-icon> </div> </div>  <div ng-switch-when=url editable-url=$parent.myDataModel onaftersave=$parent.ok($data) layout=row ng-class=\"{'my-editable' : $parent.amWbInlineEnable}\" md-colors=\"::{borderColor: 'primary-100'}\"> <ng-transclude flex></ng-transclude> <div class=xedit-icon ng-show=$parent.amWbInlineEnable md-colors=\"::{color: 'primary',backgroundColor: 'primary-100', borderColor: 'primary-100'}\"> <wb-icon>edit</wb-icon> </div> </div>  <div ng-switch-when=search editable-search=$parent.myDataModel onaftersave=$parent.ok($data) layout=row ng-class=\"{'my-editable' : $parent.amWbInlineEnable}\" md-colors=\"::{borderColor: 'primary-100'}\"> <ng-transclude flex></ng-transclude> <div class=xedit-icon ng-show=$parent.amWbInlineEnable md-colors=\"::{color: 'primary',backgroundColor: 'primary-100', borderColor: 'primary-100'}\"> <wb-icon>edit</wb-icon> </div> </div>  <div ng-switch-when=color editable-color=$parent.myDataModel onaftersave=$parent.ok($data) layout=row ng-class=\"{'my-editable' : $parent.amWbInlineEnable}\" md-colors=\"::{borderColor: 'primary-100'}\"> <ng-transclude flex></ng-transclude> <div class=xedit-icon ng-show=$parent.amWbInlineEnable md-colors=\"::{color: 'primary',backgroundColor: 'primary-100', borderColor: 'primary-100'}\"> <wb-icon>edit</wb-icon> </div> </div>  <div ng-switch-when=date editable-date=$parent.myDataModel onaftersave=$parent.ok($data) layout=row ng-class=\"{'my-editable' : $parent.amWbInlineEnable}\" md-colors=\"::{borderColor: 'primary-100'}\"> <ng-transclude flex></ng-transclude> <div class=xedit-icon ng-show=$parent.amWbInlineEnable md-colors=\"::{color: 'primary',backgroundColor: 'primary-100', borderColor: 'primary-100'}\"> <wb-icon>edit</wb-icon> </div> </div>  <div ng-switch-when=time editable-time=$parent.myDataModel onaftersave=$parent.ok($data) layout=row ng-class=\"{'my-editable' : $parent.amWbInlineEnable}\" md-colors=\"::{borderColor: 'primary-100'}\"> <ng-transclude flex></ng-transclude> <div class=xedit-icon ng-show=$parent.amWbInlineEnable md-colors=\"::{color: 'primary',backgroundColor: 'primary-100', borderColor: 'primary-100'}\"> <wb-icon>edit</wb-icon> </div> </div>  <div ng-switch-when=month editable-month=$parent.myDataModel onaftersave=$parent.ok($data) layout=row ng-class=\"{'my-editable' : $parent.amWbInlineEnable}\" md-colors=\"::{borderColor: 'primary-100'}\"> <ng-transclude flex></ng-transclude> <div class=xedit-icon ng-show=$parent.amWbInlineEnable md-colors=\"::{color: 'primary',backgroundColor: 'primary-100', borderColor: 'primary-100'}\"> <wb-icon>edit</wb-icon> </div> </div>  <div ng-switch-when=week editable-week=$parent.myDataModel onaftersave=$parent.ok($data) layout=row ng-class=\"{'my-editable' : $parent.amWbInlineEnable}\" md-colors=\"::{borderColor: 'primary-100'}\"> <ng-transclude flex></ng-transclude> <div class=xedit-icon ng-show=$parent.amWbInlineEnable md-colors=\"::{color: 'primary',backgroundColor: 'primary-100', borderColor: 'primary-100'}\"> <wb-icon>edit</wb-icon> </div> </div>  <div ng-switch-when=password editable-password=$parent.myDataModel onaftersave=$parent.ok($data) layout=row ng-class=\"{'my-editable' : $parent.amWbInlineEnable}\" md-colors=\"::{borderColor: 'primary-100'}\"> <ng-transclude flex></ng-transclude> <div class=xedit-icon ng-show=$parent.amWbInlineEnable md-colors=\"::{color: 'primary',backgroundColor: 'primary-100', borderColor: 'primary-100'}\"> <wb-icon>edit</wb-icon> </div> </div>  <div ng-switch-when=datetime editable-datetime-local=$parent.myDataModel onaftersave=$parent.ok($data) layout=row ng-class=\"{'my-editable' : $parent.amWbInlineEnable}\" md-colors=\"::{borderColor: 'primary-100'}\"> <ng-transclude flex></ng-transclude> <div class=xedit-icon ng-show=$parent.amWbInlineEnable md-colors=\"::{color: 'primary',backgroundColor: 'primary-100', borderColor: 'primary-100'}\"> <wb-icon>edit</wb-icon> </div> </div>  <div ng-switch-when=file editable-file=$parent.myDataModel onaftersave=$parent.ok($data) layout=row ng-class=\"{'my-editable' : $parent.amWbInlineEnable}\" md-colors=\"::{borderColor: 'primary-100'}\"> <ng-transclude flex></ng-transclude> <div class=xedit-icon ng-show=$parent.amWbInlineEnable md-colors=\"::{color: 'primary',backgroundColor: 'primary-100', borderColor: 'primary-100'}\"> <wb-icon>edit</wb-icon> </div> </div> </div>"
  );

}]);
