/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialWeburgerCommon')

/**
 * @ngdoc directive
 * @name amWbInline
 * @description # amWbInline
 */
.directive('amWbInline', function($q, $parse, $resource) {

	/**
	 * Link data and view
	 */
	function postLink(scope, attr, elem, ngModel) {
		scope.$watch(function(){
			return ngModel.$modelValue;
		}, function(value) {
			scope.myDataModel = value;
		});
		scope.$watch('myDataModel', function(value){
			ngModel.$setViewValue(value);
		})
		scope.myDataModel = ngModel.$viewValue;
		
		scope.ok = function(d){
			ngModel.$setViewValue(d);
			if(typeof scope.amWbInlineOnSave !== 'undefined'){
				scope.$data = d;
				scope.amWbInlineOnSave(scope);				
			}
		}
		
		/**
		 * Select image url
		 */
		scope.updateImage = function(){
			return $resource.get('image', {
				style : {
					title : 'Select image'
				},
				data : scope.myDataModel
			}) //
			.then(function(url){
				scope.ok(url);
			});
		}
	}
	
	return {
		restrict : 'E',
		transclude : true,
		replace: true,
		require: '^ngModel',
		scope: {
			amWbInlineType: '@',
			amWbInlineEnable: '<',
			amWbInlineLabel: '@?',
			amWbInlineOnSave: '&?'
		},
		templateUrl : 'views/directives/am-wb-inline.html',
		link: postLink
	};
});
