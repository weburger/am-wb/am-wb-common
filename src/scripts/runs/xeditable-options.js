/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialWeburgerCommon')
/**
 * تنظیمات مربوط به افزونه xeditable
 * 
 */
.run(function(editableOptions, editableThemes, $rootScope) {
//	editableOptions.theme = 'default'; // Default theme. Can be also 'bs2', 'bs3'
	editableThemes['angular-material'] = {
		formTpl:      '<form class="editable-wrap"></form>',
		noformTpl:    '<span class="editable-wrap"></span>',
		controlsTpl:  '<md-input-container class="editable-controls" ng-class="{\'md-input-invalid\': $error}"></md-input-container>',
		inputTpl:     '',
		errorTpl:     '<div ng-messages="{message: $error}"><div class="editable-error" ng-message="message">{{$error}}</div></div>',
		buttonsTpl:   '<span class="editable-buttons"></span>',
		submitTpl:    '<md-button type="submit" class="md-primary">save</md-button>',
		cancelTpl:    '<md-button type="button" class="md-warn" ng-click="$form.$cancel()">cancel</md-button>'
	};

	editableThemes['angular-material'].submitTpl = '<md-button class="md-icon-button md-primary" ng-click="$form.$submit()"><wb-icon>done<wb-icon></md-button>';
	editableThemes['angular-material'].cancelTpl = '<md-button class="md-icon-button md-warn" ng-click="$form.$cancel()"><wb-icon>cancel<wb-icon></md-button>';
	editableOptions.theme = 'angular-material';
//	editableOptions.isDisabled="false";
	editableOptions.iconSet="font-awesome";
	
});
