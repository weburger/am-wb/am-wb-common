/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialWeburgerCommon')
/**
 * @ngdoc module
 * @name ngDonate
 * @description
 * 
 */
.run(function($widget) {

	$widget.newWidget({
		type: 'CommonFeatureList',
		templateUrl : 'views/am-wb-common-widgets/feature-list.html',
		label : 'Vertical feature list',
		description : 'Vertical feature list',
		icon : 'wb-common-link',
		help : 'https://gitlab.com/weburger/am-wb-common/wikis/home',
		controller: 'AmWbCommonFeaturesCtrl',
		setting:['text', 'common-features', ],
	});

	$widget.newWidget({
		type: 'CommonFeatureLinks',
		templateUrl : 'views/am-wb-common-widgets/feature-links.html',
		label : 'Feature link',
		description : 'Vertical features link list',
		icon : 'wb-common-link',
		help : 'http://dpq.co.ir',
		controller: 'AmWbCommonFeaturesCtrl',
		setting:['text', 'common-features'],
	});

	$widget.newWidget({
		type: 'CommonFeatureMozaic',
		templateUrl : 'views/am-wb-common-widgets/feature-mozaic.html',
		label : 'Feature mozaic',
		description : 'Vertical features mozaic',
		icon : 'wb-common-link',
		help : 'http://dpq.co.ir',
		controller: 'AmWbCommonFeaturesCtrl',
		setting:['text', 'common-features'],
	});

	$widget.newWidget({
		type: 'CommonActionCall',
		templateUrl : 'views/am-wb-common-widgets/action-call.html',
		label : 'Action call',
		description : 'Call action with brand, title and text.',
		icon : 'wb-common-link',
		help : 'http://dpq.co.ir',
		controller: 'AmWbCommonFeaturesCtrl',
		setting:['description', 'common-features',],
	});

	$widget.newWidget({
		type: 'CommonFeatureToolbar',
		templateUrl : 'views/am-wb-common-widgets/feature-toolbar.html',
		label : 'Toolbar',
		description : 'A toolbar to show actions with sidenav.',
		icon : 'wb-common-toolbar',
		help : 'https://gitlab.com/weburger/am-wb-common/wikis/toolbar',
		controller: 'AmWbCommonFeaturesCtrl',
		setting:['description','common-features',],
	});
	
	$widget.newWidget({
		type: 'CommonVideoPlayer',
		templateUrl : 'views/am-wb-common-widgets/video-player.html',
		label : 'Video Player',
		description : 'A video player component.',
		icon : 'wb-common-video',
		help : 'https://gitlab.com/weburger/am-wb-common/wikis/video-player',
		controller: 'AmWbCommonVideoCtrl',
		setting:['common-video-player'],
	});
	$widget.newWidget({
		type: 'CommonAudioPlayer',
		templateUrl : 'views/am-wb-common-widgets/audio-player.html',
		label : 'Audio Player',
		description : 'An audio player component.',
		icon : 'wb-common-audio',
		help : 'https://gitlab.com/weburger/am-wb-common/wikis/audio-player',
		controller: 'AmWbCommonAudioCtrl',
		setting:['common-audio-player'],
	});
});
