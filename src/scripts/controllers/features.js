/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 weburger
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
/**
 * 
 */
angular.module('ngMaterialWeburgerCommon')
/**
 * 
 */
.controller('AmWbCommonFeaturesCtrl', function($scope, $wbUi, $window, $resource) {
	var ngModel = $scope.wbModel;

	function addFeature(){
		ngModel.features.push({
			'icon' : 'assignment_turned_in',
			'title' : 'Feature title',
			'text' : 'Feature description.'
		});
	}

	function removeFeature(feature) {
		var index = ngModel.features.indexOf(feature);
		if (index > -1) {
			ngModel.features.splice(index, 1);
		}
	}

	function removeAllFeatures(){
		ngModel.features = [];
	}
	
	function editFeature(feature) {
		$wbUi.openDialog({
			controller : 'AmWbCommonDialogmodelCtrl',
			templateUrl : 'views/am-wb-common-dialogs/feature.html',
			parent : angular.element(document.body),
			clickOutsideToClose : true,
			locals : {
				model : feature,
				style : {
					title : 'service'
				}
			}
		});
	}

	function moveUpFeature(feature) {
		var index = ngModel.features.indexOf(feature);
		if(index == 0){
			return;
		}
		moveArrayItem(ngModel.features, index, index - 1);
	}

	function moveDownFeature(feature) {
		var index = ngModel.features.indexOf(feature);
		if(index == ngModel.features.length - 1){
			return;
		}
		moveArrayItem(ngModel.features, index, index + 1);
	}

	function moveArrayItem(array, old_index, new_index) {
		if (new_index >= array.length) {
			var k = new_index - array.length;
			while ((k--) + 1) {
				array.push(undefined);
			}
		}
		array.splice(new_index, 0, array.splice(old_index, 1)[0]);
	};

	/*
	 * run feature action
	 */
	function runAction(feature) {
		if (feature.action.type==='link') {
			$window.location = feature.action.link;
			return;
		}
	}
	
	/**
	 * Selects featrue image
	 * 
	 * @param feature
	 * @returns
	 */
	function selectImage(feature) {
		return $resource.get('image')//
		.then(function(value){
			feature.image = value;
		});
	}

	/*
	 * Listen model
	 */
	$scope.$watch('wbModel', function() {
		if(angular.isDefined($scope.wbModel)){
			ngModel = $scope.wbModel;
			if (!angular.isDefined(ngModel.features)) {
				ngModel.features = [];
			}
		}
	});

	// Global functions
	$scope.addFeature = addFeature;
	$scope.removeFeature = removeFeature;
	$scope.removeAllFeatures = removeAllFeatures;
	$scope.editFeature = editFeature;
	$scope.moveUpFeature = moveUpFeature;
	$scope.moveDownFeature = moveDownFeature;
	$scope.runAction = runAction;
	$scope.selectImage = selectImage;
});
