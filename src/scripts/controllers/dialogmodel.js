'use strict';

angular.module('ngMaterialWeburgerCommon')
/**
 * @ngdoc function
 * @name digidociMainApp.controller:DialogmodelCtrl
 * @description # DialogmodelCtrl Controller of the digidociMainApp
 */
.controller('AmWbCommonDialogmodelCtrl', function($scope, $mdDialog, style, model) {
	$scope.model = model;
	$scope.style = style;
	$scope.hide = function() {
		$mdDialog.hide();
	};
	$scope.cancel = function() {
		$mdDialog.cancel();
	};
	$scope.answer = function(a) {
		$mdDialog.hide(a);
	};
});
